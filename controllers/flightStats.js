const express = require('express')
const api = express.Router()

api.get("/flightsStatistics",(req,res)=>{
    res.render("statistics/FlightStats.ejs");
})

api.get("/pilotsStatistics",(req,res)=>{
    res.render("statistics/pilots.ejs");
})

api.get("/planesStatistics",(req,res)=>{
    res.render("statistics/planes.ejs");
})

module.exports = api