/**
 * @index.js - manages all routing
 *
 * router.get when assigning to a single request
 * router.use when deferring to a controller
 *
 * @requires express
 */

const express = require('express')
const LOG = require('../utils/logger.js')

LOG.debug('START routing')
const router = express.Router()

// Manage top-level request first
router.get('/', (req, res, next) => {
  LOG.debug('Request to /')

  var url = req.protocol + '://' + req.get('host') + req.originalUrl;
  res.render('index.ejs', { title: 'Express App', currentUrl: url })
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/flight', require('../controllers/flight.js'))
router.use('/pilot', require('../controllers/pilot.js'))
router.use('/statistics',require('../controllers/flightStats.js'))

LOG.debug('END routing')
module.exports = router
